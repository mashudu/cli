module gitlab.wikimedia.org/releng/cli

require (
	bou.ke/staticfiles v0.0.0-20210106104248-dd04075d4104
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/ahmetb/govvv v0.3.0
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/chroma v0.9.2 // indirect
	github.com/alecthomas/colour v0.1.0 // indirect
	github.com/alecthomas/repr v0.0.0-20210801044451-80ca428c5142 // indirect
	github.com/blang/semver v3.5.1+incompatible
	github.com/briandowns/spinner v1.16.0
	github.com/containerd/containerd v1.5.5 // indirect
	github.com/docker/docker v20.10.8+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.0 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/juju/ansiterm v0.0.0-20210706145210-9283cdf370b5 // indirect
	github.com/manifoldco/promptui v0.8.0
	github.com/microcosm-cc/bluemonday v1.0.15 // indirect
	github.com/moby/term v0.0.0-20201216013528-df9cb8a40635 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/profclems/glab v1.21.1
	github.com/rhysd/go-github-selfupdate v1.2.3
	github.com/rivo/tview v0.0.0-20210923051754-2cb20002bc4c // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/spf13/cobra v1.2.1
	github.com/txn2/txeh v1.3.0
	github.com/ulikunitz/xz v0.5.10 // indirect
	github.com/xanzy/go-gitlab v0.51.1
	github.com/yuin/goldmark v1.4.1 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20210924151903-3ad01bbaa167 // indirect
	golang.org/x/oauth2 v0.0.0-20210819190943-2bc19b11175f // indirect
	golang.org/x/sys v0.0.0-20210923061019-b8560ed6a9b7 // indirect
	golang.org/x/term v0.0.0-20210916214954-140adaaadfaf
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	google.golang.org/genproto v0.0.0-20210924002016-3dee208752a0 // indirect
)

go 1.13
